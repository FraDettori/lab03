class Game {

 private int frame;
 private int pin;
 private int score;
 private int shot_after_strike;
 private int shot_after_spare;
 private int shot_for_frame;
 private int strike_consecutive;


 public Game() {
  this.pin = 10;
  this.frame = 1;
  this.score = 0;
  this.shot_after_strike = 0;
  this.shot_after_spare = 0;
  this.shot_for_frame = 1;
  this.strike_consecutive = 0;
 }

 public void roll(int pins){

         if (shot_after_strike == 1 || shot_after_strike == 2) {
             if (pins == 10) {
                 strike_consecutive++;
                 this.strike(1);
             } else {
                 strike_consecutive = 0;
                 score += pins;
                 score += pins;
                 if(shot_after_strike == 1 && frame >= 10){
                     score += pins;
                 }
                 if(shot_after_spare == 1){
                     score += pins;
                     shot_after_spare++;
                 }
                 pin = pin - pins;
                 if (pin == 0){
                     this.spare();
                 }else if (shot_for_frame > 2){
                     pin = 10;
                     shot_for_frame = 1;
                 }else{
                     shot_for_frame++;
                 }
                 shot_after_strike++;

             }

         } else if (pins == 10) {
             this.strike(0);
         } else {
             score += pins;
             if(shot_after_spare == 1){
                 score += pins;
                 shot_after_spare++;
             }
             pin = pin - pins;
             if (pin == 0){
                 this.spare();
             }else if (shot_for_frame > 2){
                 pin = 10;
                 shot_for_frame = 1;
             }else{
                 shot_for_frame++;
             }
         }
         frame++;

 }

 public void strike (int number_of_strike){
     if (frame >= 10){
        strike_consecutive=0;
     }
     if(number_of_strike > 0){
         score += 10;
         if (strike_consecutive >= 2){
             score += 10;
         }
     }
     score += 10;
     shot_after_strike = 1;
 }

 public void spare (){
     shot_after_spare = 1;
     pin = 10;
 }

 public int score(){
     pin = 10;
     return this.score;
 }

}